<!DOCTYPE html>
<html lang="en">
<head>
    <title>$Title$</title>
<#include "micro-header.ftl">
</head>
<style>
    li {
        word-wrap: break-word
    }

    body, td, th {
        font-family: Tahoma, Arial, ”Helvetica Neue“, ”Hiragino Sans GB”, Simsun, sans-serif;
    }

</style>
<body>
<div class="container">
<#include "nav-bar.ftl">
    <div class="row">
        <div id="show-user-info">
            <div class="col-md-6">
                <h2>用户信息展示</h2>
                <ul id="user-info" class="list-group">
                    用户名
                    <li id="username" class="list-group-item"></li>
                    Email
                    <li id="email" class="list-group-item"></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 ">
            <h2>头像信息</h2>
            <img id="avatar" src="" alt="" class="img-responsive"/>
            <ul id="header-info" class="list-group">
                TOKEN1
                <li id="token-jwt" class="list-group-item"></li>
                TOKEN2
                <li id="token-oauth" class="list-group-item"></li>
            </ul>
        </div>
    </div>
</div>



<#include "micro-footer.ftl">
<script>

    /**
     * 获取用户信息
     */
    var getUserInfo = function () {
        //先清空
        $('#avatar').hide();
        $('#user-info').find("li").empty();
        $('#header-info').find("li").empty();
        //再设置
        if (getJwtToken() !== null) {
            $("#token-jwt").text(getJwtToken());
            setJwtUserInfo();
        } else if (getOAuthToken() !== null) {
            $("#token-oauth").text(getOAuthToken());
            seAuthUserInfo();
        }
    };

    var seAuthUserInfo = function () {
        $.ajax({
            url: "https://api.github.com/user",
            type: "GET",
            headers: createOAuthTokenHeader(),
            success: function (data, textstatus, xhr) {
                $('#username').text(data.login);
                $('#avatar')
                        .show()
                        .attr('src', data.avatar_url)
                        .attr('alt', data.name)
                        .attr('width', '100em');
            }
        })
    };

    var setJwtUserInfo = function () {
        $.ajax({
            url: "/user",
            type: "GET",
            headers: createJwtTokenHeader(),
            success: function (data, status, xhr) {
                $('#username').text(data.username);
                $('#email').text(data.email);
            }
        })
    };

    $(function () {
        getUserInfo();
    });
</script>
</body>
</html>