<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/webjars/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/webjars/bootstrap/css/bootstrap-theme.min.css"/>
<link rel="stylesheet" type="text/css" href="/webjars/font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="${request.contextPath}/css/captcha.css"/>
<link rel="stylesheet" type="text/css" href="${request.contextPath}/layui/css/layui.css"/>
<link rel="stylesheet" type="text/css" href="${request.contextPath}/layui/css/layui.mobile.css"/>
