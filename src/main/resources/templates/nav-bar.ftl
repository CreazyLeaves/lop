<#-- navbar-static-top 随着页面一起滚动 -->
<div class="row">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#my-navbar-collapse">
                    <span class="sr-only">切换导航</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/#">TITLE</a>
            </div>
            <div class="collapse navbar-collapse" id="my-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="register" href="/register">注册</a></li>
                    <li><a class="login" href="/login">登录</a></li>
                    <li><a href="#" class="logout" id="logout">注销</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>