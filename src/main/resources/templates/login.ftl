<!DOCTYPE html>
<html lang="en">
<head>
    <title>登录页面</title>
<#include "micro-header.ftl">
</head>
<style>
    @media screen and (min-device-width: 600px) and (max-device-width: 1600px) {
        .my-form {
            margin: 9.2em auto 10em 5em;
        }

        .my-input {
            width: 18em;
        }
    }

    @media screen and (max-device-width: 600px) {

        .my-form {
            width: 24em;
            margin-top: 6.2em;
            margin-bottom: 0.2em;
        }
    }

    li {
        word-wrap: break-word
    }

    body, td, th {
        font-family: Tahoma, Arial, ”Helvetica Neue“, ”Hiragino Sans GB”, Simsun, sans-serif;
    }

    .center {
        width: auto;
        display: table;
        margin-left: auto;
        margin-right: auto;
    }

    .text-center {
        text-align: center;
    }
</style>
<body>
<div class="container">
<#include "nav-bar.ftl">
    <div class="row">
        <form id="loginForm" class="layui-form my-form"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
            <div class="layui-form-item">
                <label class="layui-form-label"><i class="fa fa-user fa-fw"></i></label>
                <div class="layui-input-block">
                    <input type="text" name="username" placeholder="username" autocomplete="off" autofocus
                           class="layui-input my-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"><i class="fa fa-key fa-fw"></i></label>
                <div class="layui-input-block ">
                    <input type="password" name="password" placeholder="password" autocomplete="off"
                           class="layui-input my-input">
                </div>
            </div>
            <div class="layui-form-item" id="captcha-area" hidden>
                <label class="layui-form-label">验证码</label>
                <div class="layui-input-inline captcha-row-l layui-input-captcha-l">
                    <input type="text" id="captcha-input" onkeyup="validate()" maxlength="4" placeholder="验证码"
                           autocomplete="off" class="layui-input captcha-input"/>
                </div>
                <div class="layui-input-inline captcha-row-r layui-input-captcha-r">
                    <input type="text" id="captcha" onclick="createCaptcha()" placeholder="验证码"
                           class="layui-input" readonly/>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn login-btn" lay-submit lay-filter="*">LOGIN</button>
                    <button id="oauthLogin" class="layui-btn layui-btn-primary">GITHUB</button>
                </div>
            </div>
        </form>
    </div>
</div>
<#include "micro-footer.ftl">
<script>
    /**
     * 表单登录
     */
    $("#loginForm").submit(function (event) {
        //阻止默认的表单提交。使用js提交
        event.preventDefault();

        if (getOAuthToken() === null && getJwtToken() === null) {
            var $form = $(this);
            var formData = {
                username: $form.find('input[name="username"]').val(),
                password: $form.find('input[name="password"]').val()
            };
            validataLogin(formData);
        } else {
            layer.alert('您已登录,请注销后再次尝试登录', {
                btnAlign: 'c'
            });
        }
    });


    /**
     * 获取 jwtToken,并验证 jwtToken的合法性
     */
    var timer = 0;
    function doLogin(loginData) {
        $.ajax({
            url: "/jwtToken",
            type: "POST",
            data: JSON.stringify(loginData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                setJwtToken(data.token);
                location.href = "/";    //直接跳转到成功页面
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (timer <= 7) {
                    timer++;
                } else {
                    showCaptcha();
                }
                if (jqXHR.status === 401) {
                    layer.alert('用户名或密码错误', {
                        btnAlign: 'c'
                    });
                } else {
                    throw new Error("an unexpected error occured: " + errorThrown);
                }
            }
        });
    }

    /**
     * oauth 登录
     */
    $('#oauthLogin').click(function (event) {
        event.preventDefault();
        if (getOAuthToken() === null && getJwtToken() === null) {
            location.href = "/login/github";
        } else {
            layer.alert('您已登录,请注销后再次尝试登录', {
                btnAlign: 'c'
            });
        }
    });
</script>
</body>
</html>