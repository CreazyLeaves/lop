<!DOCTYPE html>
<html lang="en">
<head>
    <title>注册信息</title>
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>
<div class="container">
    <div class="row">
        <br><br><br><br>
        <h4>验证信息：</h4>
    </div>
    <div class="row">
    <#if username?? && msg??>
    ${username}!，欢迎访问本网站。${msg}
    <#else >
        请访问邮箱进行验证
    </#if>
    <#if redirectUrl??>
        <a href="${redirectUrl}">点击登录</a>
    </#if>
    </div>
</div>
</body>
</html>