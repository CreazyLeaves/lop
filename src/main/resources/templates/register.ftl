<!DOCTYPE html>
<html lang="en">
<head>
<#include "micro-header.ftl">
    <title>注册表单</title>
<#include "micro-header.ftl">
</head>
<style>
    @media screen and (min-device-width: 600px) and (max-device-width: 1600px) {
        .my-form {
            margin: 2.2em auto;
        }

        .my-input {
            width: 18em;
        }
    }

    @media screen and (max-device-width: 600px) {

        .my-form {
            width: 24em;
            margin-top: 3.2em;
            margin-bottom: 0.2em;
        }
    }
</style>
<body>
<div class="container">
<#include "nav-bar.ftl">
    <div class="row">
        <form id="registerForm" name="regForm" class="layui-form my-form">
            <div class="layui-form-item">
                <label class="layui-form-label">账 号</label>
                <div class="layui-input-inline">
                    <input type="text" name="username" required lay-verify="required" placeholder="请输入您的用户名"
                           autocomplete="off" class="layui-input my-input">
                </div>
            </div>
        <#-- 行内元素 添加验证码-->
            <div class="layui-form-item">
                <label class="layui-form-label">密码</label>
                <div class="layui-input-inline">
                    <input type="password" name="password" required lay-verify="pass" placeholder="密码必须6到12位"
                           autocomplete="off" class="layui-input my-input">
                </div>
            <#--<div class="layui-form-mid layui-word-aux">辅助文字</div>-->
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">确认密码</label>
                <div class="layui-input-inline">
                    <input type="password" name="password2" required lay-verify="pass2" placeholder="再次输入一遍密码"
                           autocomplete="off" class="layui-input my-input">
                </div>
                <#--<div class="layui-form-pane layui-word-aux">确认密码</div>-->
            </div>
        <#-- 行内元素 -->
            <div class="layui-form-item">
                <label class="layui-form-label">性 别</label>
                <div class="layui-input-inline">
                    <input type="radio" name="sex" value="男" title="男">
                    <input type="radio" name="sex" value="女" title="女" checked>
                </div>
            </div>
            <input type="hidden" name="address"/>
            <div class="layui-form-item">
                <label class="layui-form-label">所在地</label>
                <div class="layui-input-inline">
                    <select name="provid" id="provid" lay-filter="provid">
                        <option value="">请选择省</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select name="cityid" id="cityid" lay-filter="cityid">
                        <option value="">请选择市</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select name="areaid" id="areaid" lay-filter="areaid">
                        <option value="">请选择县/区</
                        option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">邮 箱</label>
                <div class="layui-input-inline">
                    <input type="text" name="email" onkeyup="showCaptcha()" lay-verify="email" placeholder="请输入有效邮箱"
                           autocomplete="off" class="layui-input my-input">
                </div>
            </div>
            <div class="layui-form-item" id="captcha-area" hidden>
                <label class="layui-form-label">验证码</label>
                <div class="layui-input-inline captcha-row-l">
                    <input type="text" id="captcha-input" onkeyup="validate()" maxlength="4" placeholder="验证码"
                           autocomplete="off" class="layui-input captcha-input"/>
                </div>
                <div class="layui-input-inline captcha-row-r">
                    <input type="text" id="captcha" onclick="createCaptcha()" placeholder="验证码"
                           class="layui-input" readonly/>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-button">
                    <button id="register" class="layui-btn" lay-submit>立即提交</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </div>
</div>
<#include "micro-footer.ftl">
<script>
    /**
     * 表单注册
     */
    $("#registerForm").submit(function (event) {
        //阻止默认的表单提交。使用js提交
        event.preventDefault();

        //先去查询账号是否已经存在，再进行序列化注册。
        var $form = $(this);
        var formData = {
            username: $form.find('input[name="username"]').val(),
            password: $form.find('input[name="password"]').val(),
            email: $form.find('input[name="email"]').val(),
            age: 100,
            sex: $form.find('input[name="sex"]:checked').val(),
            address: $('select#provid option:selected').val() + "," + $('select#cityid option:selected').val() + "," + $('select#areaid option:selected').val(),
            headerImage: "#"
        };
        validataRegister(formData);
    });

    function doRegister(registerData) {

        $.ajax({
            url: "/userRegister",
            type: "POST",
            data: JSON.stringify(registerData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result, textStatus, jqXHR) {
                if (result.status !== 500) {
                    //不为500，发送邮件
                    doMail();
                    document.regForm.reset();
                } else {
                    layer.alert("注册失败, 账号已经存在", {
                        btnAlign: 'c'
                    });
                    createCaptcha();
                    $('#captcha-input').val("");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

                throw new Error("an unexpected error occured: " + errorThrown);
            }
        });
    }

    //先注册用户，后激活邮件
    function doMail() {

        $.ajax({
            url: '/sendMail',
            type: "POST",
            data: JSON.stringify({
                username: $form.find('input[name="username"]').val(),
                email: $form.find('input[name="email"]').val()
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result, textStatus, jqXHR) {
                if (result.status !== 500) {
                    //跳转
                    //location.href = "/邮箱等待验证页面";
                    layer.alert('请访问邮箱，进行验证', {
                                btnAlign: 'c',
                                closeBtn: 0,
                                anim: 4,   //动画类型
                                yes: function (index, layero) {
                                    //do something
                                    layer.close(index); //如果设定了yes回调，需进行手工关闭
                                    location.href="/";
                                }
                            }
                    );
                }
                else {
                    //layer.alert("邮件未发送");
                }
            }
        })
        ;
    }
</script>
</body>
</html>