var $notLoggedIn = $("#notLoggedIn");
var $loggedIn = $("#loggedIn").hide();
var $response = $("#response");
var $login = $("#login");
var $userInfo = $("#userInfo").hide();
var JWT_TOKEN_KEY = "jwtToken";
var OAUTH_TOKEN_KEY = "oauthToken";

function setJwtToken(token) {
    localStorage.setItem(JWT_TOKEN_KEY, token);
}

function getJwtToken() {
    return localStorage.getItem(JWT_TOKEN_KEY);
}


function removeJwtToken() {
    localStorage.removeItem(JWT_TOKEN_KEY);
}

function setOAuthToken(token) {
    localStorage.setItem(OAUTH_TOKEN_KEY, token);
}

function getOAuthToken() {
    //创建 token,并设置token
    requireAndSetOAuthToken();
    return localStorage.getItem(OAUTH_TOKEN_KEY);
}

function removeOAuthToken() {
    localStorage.removeItem(OAUTH_TOKEN_KEY);
}

/**
 * 获取 oauthToken
 */
function requireAndSetOAuthToken() {

    $.ajax({
        url: '/oauthToken',
        type: 'POST',
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                setOAuthToken(data.access_token);
            }
        }
    });
}

/**
 * 表单注销
 */
function doLogout() {
    removeJwtToken();
    removeOAuthToken();
}

/**
 * 将 token放到授权头中,服务端可以从请求头中获取token 进行验证.
 * @returns {*}
 */
function createJwtTokenHeader() {
    var token = getJwtToken();
    if (token) {
        return {"Authorization": token};
    } else {
        return {};
    }
}

function createOAuthTokenHeader() {

    var token = getOAuthToken();
    if (token) {
        return {"Authorization": "token " + token};
    } else {
        return {};
    }
}


/**
 * 页面跳转
 */
var jumpToIndex = setTimeout(function () {
    var interval = 5;
    if (interval > 0) {
        setTimeout(arguments.callee, 1000);
        // $('em').text(interval);
        interval--;
    } else {
        window.location.href = "/";
    }
}, 1000);


/* **************************************.ftl************************************/

/**
 * 用户注销
 */
$('#logout').click(function (event) {
//        event.preventDefault();
    doLogout();          //jwt 和oauth2登录注销
    $.getJSON("/clearSession");
    layer.alert('注销成功', {
        btnAlign: 'c'
    });
    //重置 用户信息
    getUserInfo();
});


/* **************************************.ftl************************************/
layui.use('layer', function () {
    var layer = layui.layer;
});

layui.use(['layer', 'form'], function () {
    var layer = layui.layer, form = layui.form();

    form.verify({

        pass: [
            /^[\S]{6,12}$/
            , '密码必须6到12位，且不能出现空格'
        ],

        pass2: function (password2) {
            var password = $('.layui-form-item input[name=password]').val();
            if (password !== password2) {
                return '两次输入的密码不一致，请重新确认';
            }
        }
    });

});

// function ensurePassword() {
//     var password = $('.layui-form-item input[name=password]').val();
//     var password2 = $('.layui-form-item input[name=password2]').val();
//     if (password2.length >= password.length && password !== password2) {
//         layer.tips('密码不一致，请重新确认', '.layui-form-item input[name=password2]');
//     }
// }
