/**
 * 验证码
 * Created by lg on 2017/4/6.
 */
var captcha; //在全局定义验证码
var captchaLength = 4;//验证码的长度
window.onload = function () {
    createCaptcha();
};
//产生验证码
function createCaptcha() {
    captcha = "";
    var checkCode = document.getElementById("captcha");
    var random = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];//随机数
    for (var i = 0; i < captchaLength; i++) {//循环操作
        var index = Math.floor(Math.random() * 36);//取得随机数的索引（0~35）
        captcha += random[index];//根据索引取得随机数加到captcha上
    }
    checkCode.value = captcha;//把captcha值赋给验证码
}
//校验验证码
function validate() {
    var $captchaInput = $('#captcha-input');
    var cText = $captchaInput.val().toUpperCase(); //取得输入的验证码并转化为大写
    if (cText.length <= 0) { //若输入的验证码长度为0
        layer.tips('请输入验证码！', '#captcha'); //则弹出请输入验证码
    }
    else if (cText.length === captchaLength && cText !== captcha) { //若输入的验证码与产生的验证码不一致时
        layer.tips("验证码输入错误", '#captcha'); //则弹出验证码输入错误
        // createCaptcha();//刷新验证码
        // $captchaInput.val("");//清空文本框
    }
    else if (cText.length === captchaLength && cText === captcha) { //输入正确时
        layer.tips('ok', '#captcha');
    }
}

function showCaptcha() {
    $('#captcha-area').show();
}
function validataRegister(formData) {
    var $captchaInput = $('#captcha-input');
    var cText = $captchaInput.val().toUpperCase(); //取得输入的验证码并转化为大写
    if (cText.length <= 0) { //若输入的验证码长度为0
        layer.tips('请输入验证码！', '#captcha'); //则弹出请输入验证码
    }

    else if (cText !== captcha) { //若输入的验证码与产生的验证码不一致时
        layer.tips("验证码输入错误", '#captcha'); //则弹出验证码输入错误
        createCaptcha();//刷新验证码
        $captchaInput.val("");//清空文本框
    }
    else if (cText.length === captchaLength && cText === captcha) { //输入正确时
        doRegister(formData);
    }
}
function validataLogin(formData) {
    var $captchaArea = $('#captcha-area');
    if (!$captchaArea.is(':hidden')) {
        var $captchaInput = $('#captcha-input');
        var cText = $captchaInput.val().toUpperCase(); //取得输入的验证码并转化为大写
        if (cText.length <= 0) { //若输入的验证码长度为0
            layer.tips('请输入验证码！', '#captcha'); //则弹出请输入验证码
        }

        else if (cText !== captcha) { //若输入的验证码与产生的验证码不一致时
            layer.tips("验证码输入错误", '#captcha'); //则弹出验证码输入错误
            createCaptcha();//刷新验证码
            $captchaInput.val("");//清空文本框
        }
        else if (cText.length === captchaLength && cText === captcha) { //输入正确时
            createCaptcha();//刷新验证码
            $captchaInput.val("");//清空文本框
            doLogin(formData);
        }
    } else {
        doLogin(formData);
    }
}