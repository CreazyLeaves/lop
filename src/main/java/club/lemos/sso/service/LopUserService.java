package club.lemos.sso.service;

import club.lemos.common.pojo.CommonResult;
import club.lemos.sso.domain.LopUser;
import club.lemos.sso.domain.LopUserRequest;

public interface LopUserService {

    LopUser findByUsername(String username);

    CommonResult userRegister(LopUserRequest userRequest);

    //发送激活码
    CommonResult sendActiveCode(String activeCode,String username);

    //根据激活码激活用户
    CommonResult verifyAndActiveUser(boolean flag,String activeCode,String username);
}
