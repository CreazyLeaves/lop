package club.lemos.sso.service;

import java.util.Map;

/**
 * 邮件发送和邮件模板获取
 *
 * Created by lg on 2017/4/7.
 */
public interface FreemarkerMailService {

    void sendMail(String templateName,String toEmailAddr, String subject, Map<String,String> content);

    String getMailContent(Map<String,String> content, String templateName);

}
