package club.lemos.sso.service.impl;

import club.lemos.common.pojo.CommonResult;
import club.lemos.sso.domain.LopUser;
import club.lemos.sso.domain.LopUserRequest;
import club.lemos.sso.repository.LopUserRepository;
import club.lemos.sso.service.LopUserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

@Service
public class LopUserServiceImpl implements LopUserService {


    @Resource
    private LopUserRepository lopUserRepository;

    @Override
    public LopUser findByUsername(String username) {
        return lopUserRepository.findByUsername(username);
    }

    @Override
    public CommonResult userRegister(LopUserRequest userRequest) {
        LopUser lopUser = new LopUser(
                userRequest.getUsername(),
                new BCryptPasswordEncoder().encode(userRequest.getPassword()),
                userRequest.getEmail(),
                userRequest.getAge(),
                userRequest.getSex(),
                userRequest.getAddress(),
                userRequest.getHeaderImage()
        );
        try {
            lopUserRepository.save(lopUser);
            return CommonResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return CommonResult.build(500, "注册失败");
        }
    }

    @Override
    public CommonResult sendActiveCode(String activeCode, String username) {
        lopUserRepository.updateActiveCodeByUsername(activeCode, username);
        return CommonResult.ok();
    }

    @Override
    public CommonResult verifyAndActiveUser(boolean flag, String activeCode, String username) {
        LopUser lopUser = findByUsername(username);
        if (Objects.equals(lopUser.getActiveCode(), activeCode)) {

            lopUserRepository.updateUserEnabledByUsername(flag, username);
        }else {
            return CommonResult.build(400, "激活码不正确");
        }
        return CommonResult.ok();
    }


}
