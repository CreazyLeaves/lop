package club.lemos.sso.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * index page
 * Created by lg on 2017/3/28.
 */
@Controller
public class IndexController {

    @RequestMapping({"/","/index"})
    public String index() {
        return "index";
    }
}
