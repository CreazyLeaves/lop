package club.lemos.sso.web.controller;

import club.lemos.sso.service.LopUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;


/**
 * 负责登录
 */
@Controller
public class PagesController {

    @Resource
    private LopUserService lopUserService;

    /**
     * 登录页面
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    /**
     * 登录成功页面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/success",method = RequestMethod.GET)
    private ModelAndView loginSuccess(ModelAndView modelAndView) {
        modelAndView.setViewName("success");
        return modelAndView;
    }

    /**
     * 用户信息展示页面
     * @return
     */
    @RequestMapping(value = "/github",method = RequestMethod.GET)
    private String getInfo() {
        return "github";
    }

    /**
     * 注册页面
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    private String userRegister() {
        return "register";
    }

}
