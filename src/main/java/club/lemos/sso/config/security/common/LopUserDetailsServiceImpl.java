package club.lemos.sso.config.security.common;

import club.lemos.sso.domain.LopUser;
import club.lemos.sso.repository.LopUserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * 实现 UserDetailsService 接口
 * Created by stephan on 20.03.16.
 */
@Service
public class LopUserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private LopUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LopUser user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return LopUserDetailsFactory.create(user);
        }
    }
}
