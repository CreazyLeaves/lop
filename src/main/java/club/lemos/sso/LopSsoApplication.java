package club.lemos.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LopSsoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LopSsoApplication.class, args);
	}
}
