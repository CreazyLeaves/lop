package club.lemos.sso.domain;

public enum LopRoleName {
    ROLE_USER, ROLE_ADMIN
}