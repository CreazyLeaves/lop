package club.lemos.sso.repository;


import club.lemos.sso.domain.LopUser;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface LopUserRepository extends CrudRepository<LopUser, Long> {

    /**
     * 通过username查找用户信息
     **/
    LopUser findByUsername(String username);

    List<LopUser> findAll();

    /**
     * 更新激活码
     * @param username
     * @return
     */
    @Transactional
    @Modifying
    @Query("update LopUser u set u.activeCode = ?1 where u.username = ?2")
    int updateActiveCodeByUsername(String activeCode,String username);

    /**
     * 更新激活状态
     */
    @Transactional
    @Modifying
    @Query("update LopUser u set u.enabled = ?1 where u.username = ?2")
    int updateUserEnabledByUsername(boolean enabledFlag,String username);

}