# lop

OAuth2 与 jwt 整合 spring boot 与 Spring Security 的 Demo

## 相关配置

先创建数据库，比如名字叫 "lop"

编辑项目的配置文件 **lop/src/main/resources/config/application.properties** 

```
### datasource
spring.datasource.url=jdbc:mysql://localhost:3306/lop?useSSL=false
spring.datasource.username=your-mysql-name
spring.datasource.password=your-mysql-password
```

修改完毕之后，启动项目，会自动建表。完全启动后，打开浏览器访问 [localhost:8080](localhost:8080)
